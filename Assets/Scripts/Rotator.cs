﻿using UnityEngine;

/// <summary>
/// This is simple class to rotate an object
/// </summary>
public class Rotator : MonoBehaviour
{
    // Set rotation speed
    public float rotationSpeed = 30;

    /// <summary>
    /// Method called once per frame
    /// </summary>
    private void Update()
    {
        transform.Rotate(Vector3.up * rotationSpeed * Time.deltaTime);
    }
}
